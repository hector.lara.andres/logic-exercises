package com.nova.solutions.logic.exercises.test;

public class Fibonacci {
	
	public int serie(int n) {
		int numPosition = 0;
		if (n > 1){
			numPosition = serie(n - 1) + serie(n - 2);
	    } else if (n == 1) {
	    	numPosition = 1;
	    } else if (n == 0) {
	    	numPosition = 0;
	    } else {
	        System.out.println("Debes ingresar un tamaño mayor o igual a 1");
	        numPosition = 0; 
	    }
		
		return numPosition;
	}
	
	public static void main(String ...args) {
		Fibonacci f = new Fibonacci();
		System.out.println("Fibonacci: " + f.serie(7));
	}

}
