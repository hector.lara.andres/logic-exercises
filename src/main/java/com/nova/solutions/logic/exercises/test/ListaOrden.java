package com.nova.solutions.logic.exercises.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ListaOrden {
	
	public List<String> ordenar(List<String> list, boolean orden) {
		Collections.sort(list);
		if(orden) {
			Collections.reverse(list);
		}
		
		return list;
	}
	
	public static void main(String ...args) {
		List<String> lista = new ArrayList<String>();
		lista.add("Perro");
		lista.add("Caballo");
		lista.add("Gato");
		lista.add("Serpiente");
		lista.add("Jirafa");
		
		ListaOrden lo = new ListaOrden();
		
		for(String s: lo.ordenar(lista, true)) {
			System.out.println(s);
		}
	}

}
