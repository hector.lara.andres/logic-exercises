package com.nova.solutions.logic.exercises.test;

public class Palindromo {
	public boolean isPalindromo(String palabra) {
		String inverso = new StringBuilder(palabra!= null ? palabra : "").reverse().toString();
		return (palabra != null && !palabra.trim().equals("") && palabra.equals(inverso));
	}
	
	public static void main(String ...args) {
		Palindromo p = new Palindromo();
		
		System.out.println("¿Es palíndromo? " + p.isPalindromo("SaLaS"));
	}
}
