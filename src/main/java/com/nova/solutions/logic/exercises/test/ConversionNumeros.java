package com.nova.solutions.logic.exercises.test;

public class ConversionNumeros {
	
	public String convertir(int decimal, char tipo) {
		String valor = "";
		tipo = Character.toUpperCase(tipo);
		
		switch (tipo) {
		case 'B':
			valor = Integer.toBinaryString(decimal);
			break;
			
		case 'O':
			valor = Integer.toOctalString(decimal);
			break;
			
		case 'H':
			valor = Integer.toHexString(decimal);
			break;

		default:
			valor = String.valueOf(decimal);
			break;
		}
		
		return valor;
	}
	
	
	public static void main(String ...args) {
		ConversionNumeros cn = new ConversionNumeros();
		
		System.out.println("binario:: " + cn.convertir(12457, 'b'));
		System.out.println("octal:: " + cn.convertir(12457, 'o'));
		System.out.println("hexadecimal:: " + cn.convertir(12457, 'h').toUpperCase());
	}
}
